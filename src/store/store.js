import { writable } from 'svelte/store';

export const isNight = writable(false);

export const whatToShow = writable({
	all: true,
	active: false,
	completed: false
});

export const todos = writable([
	{
		id: 'a84f391b-d6a3-494b-ada1-3a3299e71dcd',
		text: 'Complete online Javascript course',
		completed: true
	},
	{
		id: '8c31b2c8-fa3f-4b4c-8c15-0e95b5be3900',
		text: 'Jog around the park 3x',
		completed: false
	},
	{
		id: '4a16a2d6-96f4-408c-8e70-eace159f51b6',
		text: '10 minutes meditation',
		completed: false
	},
	{
		id: 'b253fc71-2e6e-4b21-bfc7-d8d80749808b',
		text: 'Read for 1 hour',
		completed: false
	},
	{
		id: '54b74e3d-e828-4c69-8d71-47a6f2f32777',
		text: 'Pick up groceries',
		completed: false
	},
	{
		id: 'ba56302c-7a6f-44c6-aa92-c417777939e8',
		text: 'Complete Todo App on Frontend Mentor',
		completed: false
	}
]);
